#! /usr/bin/env python3
# -*- coding: utf-8 -*-

"""
    Copyright (C) 2019-2023  Jean-Christophe Francois

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""


# Imports
import sys
import os
import socket
import shutil
from datetime import datetime, timedelta
import random
import argparse
import time
import logging
import sqlite3
import re
import warnings

import feedparser
from bs4 import BeautifulSoup
from mastodon import Mastodon, MastodonError, MastodonAPIError
import requests

# Number of records to keep in db table
MAX_REC_COUNT = 50

# How many seconds to wait before giving up on a download (except video download)
HTTPS_REQ_TIMEOUT = 10

# Update from https://www.whatismybrowser.com/guides/the-latest-user-agent/
USER_AGENTS = [
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:109.0) Gecko/20100101 Firefox/119.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 14_1) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/17.1 Safari/605.1.15',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36 Edg/118.0.2088.76',
    'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36 OPR/104.0.0.0',
    'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36 Vivaldi/6.4.3160.34',
]


def shutdown(start_time, log_file_name, exit_code):
    """
    Cleanly stop execution with a message on execution duration
    Remove log messages older that duration specified in config from log file
    :param start_time: timestamp of when the execution started
    :param log_file_name: name of the log file
    :param exit_code: return value to pass to shell when exiting
    """
    logging.info('Run time : {t:2.1f} seconds.'.format(t=time.time() - start_time))
    logging.info('_____________________________________________________________________________________')
    # Close logger and log file
    logging.shutdown()

    # Remove older log messages
    # Max allowed age of log message
    max_delta = timedelta(TOML['options']['log_days'])

    # Open log file
    new_log_file_name = log_file_name + '.new'
    try:
        log_file = open(log_file_name, 'r')
    except FileNotFoundError:
        # Nothing to do if there is no log file
        exit(exit_code)

    # Check each line
    pos = log_file.tell()
    while True:
        line = log_file.readline()
        # Check if we reached the end of the file
        if not line:
            exit(exit_code)

        try:
            # Extract date on log line
            date = datetime.strptime(line[:10], '%Y-%m-%d')
        except ValueError:
            # date was not found on this line, try next one
            continue
        # Time difference between log message and now
        log_delta = datetime.now() - date
        # Only keep the number of days of the difference
        log_delta = timedelta(days=log_delta.days)
        if log_delta < max_delta:
            # Reset file pointer to position before reading last line
            log_file.seek(pos)
            remainder = log_file.read()
            output_file = open(new_log_file_name, 'w')
            output_file.write(remainder)
            output_file.close()
            # replace log file by new one
            shutil.move(new_log_file_name, log_file_name)

            break  # Exit for loop

        # Update read pointer position
        pos = log_file.tell()

    exit(exit_code)


def build_config(args):
    """
    Receives the arguments passed on the command line
    populates the TOML global dict with default values for all 'options' keys
    if a config file is provided, load the keys from the config file
    verify that a valid config is available (all keys in 'config' present)
    :param args: list of command line arguments
    """
    # Create global struct containing configuration
    global TOML

    options = {
        'include_feed_title': False,
        'max_chars': 500,
        'max_word_count': -1,
        'footer': "",
        'tweet_max_age': 1,
        'toot_cap': 0,
        'log_level': "WARNING",
        'log_days': 3,
    }

    TOML = {'config': {}, 'options': options}

    # Load config file
    toml_file = args['f']

    try:  # Included in python from version 3.11
        import tomllib
    except ModuleNotFoundError:
        # for python < 3.11, tomli module must be installed
        import tomli as tomllib

    loaded_toml = None
    # Load toml file
    try:
        with open(toml_file, 'rb') as config_file:
            loaded_toml = tomllib.load(config_file)
    except FileNotFoundError:
        print('config file not found')
        exit(-1)
    except tomllib.TOMLDecodeError:
        print('Malformed config file')
        exit(-1)

    TOML['config'] = loaded_toml['config']
    for k in TOML['options'].keys():
        try:  # Go through all valid keys
            TOML['options'][k] = loaded_toml['options'][k]
        except KeyError:  # Key was not found in file
            pass

    # Verify that we have a minimum config to run
    if 'feed_url' not in TOML['config'].keys() or TOML['config']['feed_url'] == "":
        print('CRITICAL: Missing feed URL')
        exit(-1)
    if 'mastodon_instance' not in TOML['config'].keys() or TOML['config']['mastodon_instance'] == "":
        print('CRITICAL: Missing Mastodon instance')
        exit(-1)
    if 'mastodon_user' not in TOML['config'].keys() or TOML['config']['mastodon_user'] == "":
        print('CRITICAL: Missing Mastodon user')
        exit(-1)


def login(instance, account, password):
    """
    Login to Mastodon account and return mastodon object used to post content
    :param instance: Mastodon instance to log in to
    :param account: Mastodon instance to log in to
    :param password: Password associated to account. None if not provided
    :return: mastodon object
    """
    # Create Mastodon application if it does not exist yet
    if not os.path.isfile(instance + '.secret'):
        try:
            Mastodon.create_app(
                'feedtoot',
                api_base_url='https://' + instance,
                to_file=instance + '.secret'
            )

        except MastodonError as me:
            logging.fatal('failed to create app on ' + instance)
            logging.fatal(me)
            exit(-1)

    mastodon = None

    # Log in to Mastodon instance with password
    if password is not None:
        try:
            mastodon = Mastodon(
                client_id=instance + '.secret',
                api_base_url='https://' + instance
            )

            mastodon.log_in(
                username=account,
                password=password,
                to_file=account + ".secret"
            )
            logging.info('Logging in to ' + instance)

        except MastodonError as me:
            logging.fatal('Login to ' + instance + ' Failed\n')
            logging.fatal(me)
            exit(-1)

        if os.path.isfile(account + '.secret'):
            logging.warning("""You successfully logged in using a password. An access token
                            has been saved therefore the password can be omitted from the
                            command-line in future invocations""")
    else:  # No password provided, login with token
        # Using token in existing .secret file
        if os.path.isfile(account + '.secret'):
            try:
                mastodon = Mastodon(
                    access_token=account + '.secret',
                    api_base_url='https://' + instance
                )
            except MastodonError as me:
                logging.fatal('Login to ' + instance + ' Failed\n')
                logging.fatal(me)
                exit(-1)
        else:
            logging.fatal('No secret file found. Password required to log in')
            exit(-1)

    return mastodon


def clean_html(raw_html):
    cleanr = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')
    cleantext = re.sub(cleanr, '', raw_html)
    return cleantext


def extract_img_ref(link_url):
    """ Parse HTML page linked to in given url and return twitter or facebook card image tag if found
    :param link_url: URL to parse
    :return: Parsed HTML tag found or None
    """
    try:
        r = requests.get(link_url, timeout=HTTPS_REQ_TIMEOUT)
    # Give up if anything goes wrong
    except (requests.exceptions.ConnectionError,
            requests.exceptions.Timeout,
            requests.exceptions.ContentDecodingError,
            requests.exceptions.TooManyRedirects,
            requests.exceptions.MissingSchema) as e:
        logging.debug('Could not download %s', link_url)
        logging.debug(e)
        pass
    else:
        if r.status_code == 200:
            logging.debug('Downloaded page ' + link_url)
            linked_soup = BeautifulSoup(r.text, features='html.parser')

            # Look for first 'twitter:image' meta tag in page linked in tweet text
            meta_image = linked_soup.find(name="meta", attrs={"name": "twitter:image"})

            if meta_image is None:  # Try improper 'property' attribute found used on hackster.io
                meta_image = linked_soup.find(name="meta", attrs={"property": "twitter:image"})

            if meta_image is None:  # Try facebook attribute
                meta_image = linked_soup.find(name="meta", attrs={"property": "og:image"})

            # We got something
            if meta_image is not None:
                logging.debug("Found :image meta tag in page")
                return meta_image['content'].replace('&amp;', '&')  # Replace HTML escape char
            else:
                logging.debug('No :image meta tag found in page')
                return None


def assemble_status_text(feed_title, toot, excess_chars):
    """
    Build text of status message to be posted.
    :param feed_title: Title of the feed that the entry belongs to
    :param toot: dict with the elements of the toot
    :param excess_chars: How many chars to remove from summary to fit char limit
    :return: text of status message
    """
    status = ''

    if TOML['options']['include_feed_title']:
        status += feed_title + ': '
    status += toot['title']

    # Because [:-0] returns an empty string
    if excess_chars != 0:
        status += '\n\n' + toot['summary'][:-excess_chars - 1] + "…"
    else:
        status += '\n\n' + toot['summary']

    if TOML['options']['footer'] != '':
        status += '\n' + TOML['options']['footer']
    status += '\n' + toot['link']

    # Check if status is too long
    excess_chars = len(status) - TOML['options']['max_chars']
    logging.debug("Chars in toot: " + str(len(status)))
    logging.debug("Chars max: " + str(TOML['options']['max_chars']))
    logging.debug("Chars in excess: " + str(excess_chars))
    if excess_chars > 0:
        logging.error("toot text exceeds the %i chars limit by %i chars, truncating.", TOML['options']['max_chars'], excess_chars)
        # Redo composition of status
        status = assemble_status_text(feed_title, toot, excess_chars)

    return status


def process_description(toml_file_name, soup):
    """
    parse the soup created on the description field
    of the RSS entry and generate text for the toot.
    Call function in external module if provided by the user
    :param toml_file_name: name of the TOML file
    :param soup: BeautifulSoup representation of the description
    :return: a string with the resulting text
    """
    # import module provided by user
    import importlib
    module_name = str(toml_file_name).lower().removesuffix('.toml')

    try:
        module = importlib.import_module(module_name, package=None)
    except ModuleNotFoundError:
        module = None

    if module is not None:
        return module.process_description(soup)
    else:
        return soup.get_text()


# Main
def main(argv):
    # Start stopwatch to measure execution time
    start_time = time.time()

    # Build parser for command line arguments
    parser = argparse.ArgumentParser(description='Post references to feed articles on Mastodon account.')
    parser.add_argument('-f', metavar='<.toml config file>', action='store', required=True)
    parser.add_argument('-p', metavar='<mastodon password>', action='store')

    # Parse command line
    args = vars(parser.parse_args())

    build_config(args)

    mast_password = args['p']

    log_file_name = str(args['f']).lower().replace('.toml', '.log')
    # Setup logging to file
    logging.basicConfig(
        filename=log_file_name,
        format='%(asctime)s %(levelname)-8s %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S',
    )

    log_level = logging.WARNING
    toml_log_level = TOML['options']['log_level'].upper()
    if toml_log_level == 'DEBUG':
        log_level = logging.DEBUG
    elif toml_log_level == 'INFO':
        log_level = logging.INFO
    elif toml_log_level == 'WARNING':
        log_level = logging.WARNING
    elif toml_log_level == 'ERROR':
        log_level = logging.ERROR
    elif toml_log_level == 'CRITICAL':
        log_level = logging.CRITICAL
    elif toml_log_level == 'OFF':
        # Disable all logging
        logging.disable(logging.CRITICAL)
    else:
        logging.error('Invalid log_level %s in config file. Using WARNING.', str(TOML['options']['log_level']))
    logger = logging.getLogger()
    logger.setLevel(log_level)

    logging.info('Running with the following parameters:')
    logging.info('  Config File              : ' + str(args['f']))
    logging.info('  feed_url                 : ' + TOML['config']['feed_url'])
    logging.info('  mastodon_instance        : ' + TOML['config']['mastodon_instance'])
    logging.info('  mastodon_user            : ' + TOML['config']['mastodon_user'])
    logging.info('  include_feed_title       : ' + str(TOML['options']['include_feed_title']))
    logging.info('  max_word_count           : ' + str(TOML['options']['max_word_count']))
    logging.info('  footer                   : ' + TOML['options']['footer'])
    logging.info('  tweet_max_age            : ' + str(TOML['options']['tweet_max_age']))
    logging.info('  toot_cap                 : ' + str(TOML['options']['toot_cap']))
    logging.info('  log_level                : ' + str(TOML['options']['log_level']))
    logging.info('  log_days                 : ' + str(TOML['options']['log_days']))

    # Try to open database. If it does not exist, create it
    sql = sqlite3.connect('feedtoot.db')
    db = sql.cursor()
    db.execute('''CREATE TABLE IF NOT EXISTS toots (feed_title TEXT, mastodon_instance TEXT,
               mastodon_account TEXT, timestamp FLOAT, id TEXT)''')
    db.execute('''CREATE INDEX IF NOT EXISTS hash_idx ON toots (id)''')

    # Download feed file
    socket.setdefaulttimeout(60)
    try:
        f = feedparser.parse(TOML['config']['feed_url'], agent=USER_AGENTS[random.randint(0, len(USER_AGENTS) - 1)])
    except Exception as e:
        logging.critical('Could not parse feed file:' + str(e))
        shutdown(start_time, log_file_name, -1)

    # Log that we have a bozo problem (that did not raise an exception)
    if f['bozo'] is True:
        if f['bozo_exception'] == feedparser.NonXMLContentType:
            logging.warning(str(f['bozo_exception']))

    if f.status >= 400:
        logging.critical('Could not download feed from: ' + TOML['config']['feed_url'])
        logging.critical('HTTP response: ' + str(f.status))
        shutdown(start_time, log_file_name, -1)
    else:
        logging.info('Downloaded feed ' + f.feed.title + ' from ' + TOML['config']['feed_url'])

    logging.debug('***** Feed dict: ' + str(f.feed))

    feed_title = clean_html(f.feed.title)

    try:
        feed_timestamp = time.mktime(f.feed.updated_parsed)
    except AttributeError:
        feed_timestamp = None

    # Get timestamp of last db entry
    db.execute('SELECT timestamp FROM toots ORDER BY timestamp DESC')
    last_entry_timestamp = db.fetchone()

    if last_entry_timestamp is not None:
        if feed_timestamp == last_entry_timestamp[0]:
            logging.info('Feed already processed. Stopping')
            shutdown(start_time, log_file_name, 0)

    # Process entries
    logging.info('Feed contains ' + str(len(f.entries)) + ' entries')
    toots = []
    for feed_entry in f.entries:
        toot = {}

        # Stop processing more entries if entry has been posted already
        db.execute('SELECT id FROM toots WHERE id=?', (feed_entry.id,))
        if db.fetchone() is not None:
            logging.info('Entry with id=' + feed_entry.id + ' found in database. Stopping.')
            # Stop processing more entries if entry is too old
            break

        max_age_in_hours = TOML['options']['tweet_max_age'] * 24.0
        logging.debug('feed_entry.published_parsed: ' + str(feed_entry.published_parsed))
        toot['timestamp'] = time.mktime(feed_entry.published_parsed)
        logging.debug('toot[\'timestamp\']: ' + str(toot['timestamp']))
        age_in_hours = (time.time() - toot['timestamp']) / 3600.0
        if age_in_hours > max_age_in_hours:
            logging.info('Entry with id ' + feed_entry.id + ' is too old. Stopping.')
            break

        toot['title'] = clean_html(feed_entry.title)
        toot['link'] = feed_entry.link

        # Make soup to extract content
        soup = None
        with warnings.catch_warnings():
            # Suppress warning in case description is not markup
            warnings.simplefilter("ignore")
            soup = BeautifulSoup(feed_entry.description, features='html.parser')

        # Extract url of first image in entry
        imgs = soup.find_all('img', limit=1)
        if len(imgs) == 1:
            # Don't bother if url is relative
            if imgs[0]['src'].startswith('http'):
                toot['image'] = imgs[0]['src']
                logging.debug('Image found in post')

        # If no media was specifically added in the feed, try to get the first picture
        # with "twitter:image" meta tag in the linked page of the post
        if 'image' not in toot.keys():
            logging.debug('No image in post, trying linked page')
            if toot['link'] is not None:
                img_url = extract_img_ref(toot['link'])
                if img_url is not None:
                    toot['image'] = img_url
                    logging.debug('Image found in linked page')
            else:
                logging.debug('No linked page found')

        # If still no media, try to get the first picture
        # with "twitter:image" meta tag in any linked page in post text
        if 'image' not in toot.keys():
            logging.debug('No image in linked page, trying page linked in text')
            m = re.search(r"http[^ \n\xa0]*", soup.get_text())
            if m is not None:
                link_url = m.group(0)
                if link_url.endswith(".html"):  # Only process a web page
                    img_url = extract_img_ref(link_url)
                    if img_url is not None:
                        toot['image'] = img_url
                        logging.debug('Image found in page linked in text')
            else:
                logging.debug('No linked page found in text')

        # Process description
        descr = process_description(args['f'], soup)

        # Extract first w words from description
        if TOML['options']['max_word_count'] == 0:
            toot['summary'] = ''
        elif TOML['options']['max_word_count'] > 0:
            expression = r'((?:\S+\s+){' + str(TOML['options']['max_word_count']) + '})'
            match = re.findall(expression, descr)

            if match != [] and match[0] != '':
                # Strip trailing characters and add ellipsis
                toot['summary'] = match[0].rstrip(' \n\t') + "…"
            else:
                # Use full description if no match
                toot['summary'] = descr.rstrip(' \n')
        else:
            toot['summary'] = descr.rstrip(' \n')

        toot['id'] = feed_entry.id

        logging.debug('***** toot: ' + str(toot))

        toots.append(toot)

    logging.info(str(len(toots)) + ' entries processed.')

    # Login to account on maston instance
    mastodon = None
    if len(toots) > 0:
        mastodon = login(TOML['config']['mastodon_instance'], TOML['config']['mastodon_user'], mast_password)
        logging.debug('Logged in to mastodon account')

    posted_count = 0
    for toot in reversed(toots):
        # Download image
        image = None
        media = None
        if 'image' in toot.keys():
            try:
                image = requests.get(toot['image'])
                logging.debug('Downloaded image ' + toot['image'])
            except:
                logging.debug('Failed to download image ' + toot['image'])
                pass

        # Upload image to Mastodon
        if image is not None:
            try:
                media = mastodon.media_post(image.content, mime_type=image.headers['content-type'])
                logging.debug('Uploaded image to Mastodon')
            except Exception as e:
                logging.debug('Image could not be uploaded to Mastodon')
                logging.debug(e)
                pass

        # Assemble text
        status = assemble_status_text(feed_title, toot, 0)
        logging.debug('Toot text (%i chars):\n%s', len(status), status)

        # Post toot
        try:
            if media is not None:
                mastodon.status_post(status, media_ids=[media['id']])
            else:
                mastodon.status_post(status, visibility='public')

        except MastodonAPIError as e:
            _, status_code, _, exception_message = e.args
            if status_code == 500:
                logging.error('Mastodon internal server error')
                logging.error('posting status Failed')
                continue
            elif exception_message.find('Text character limit') != -1:
                # ERROR (('Mastodon API returned error', 422, 'Unprocessable Entity', 'Validation failed: Text character limit of 500 exceeded'))
                logging.error('Toot text too long')
                logging.error('posting status Failed')
                continue
            elif exception_message.find('Try again in a moment') != -1:
                # ERROR ('Mastodon API returned error', 422, 'Unprocessable Entity', 'Cannot attach files that have not finished processing. Try again in a moment!')
                logging.warning('Mastodon API Error 422: Cannot attach files that have not finished processing. Waiting 30 seconds and retrying.')
                # Wait 30 seconds
                time.sleep(30)
                # retry posting
                try:
                    toot = mastodon.status_post(status, media_ids=[media['id']])
                except MastodonError as me:
                    logging.error('posting status Failed')
                    logging.error(me)
                else:
                    logging.warning("Retry successful")

        except MastodonError as me:
            logging.error('posting status Failed')
            logging.error(me)

        logging.debug('Posted toot on Mastodon account')

        # Insert record in database
        db.execute('INSERT INTO toots VALUES (?, ?, ?, ?, ?)',
                   (feed_title, TOML['config']['mastodon_instance'], TOML['config']['mastodon_user'], toot['timestamp'], toot['id']))
        sql.commit()
        logging.debug('Record added to database.')

        posted_count += 1
        if TOML['options']['toot_cap'] != 0 and posted_count >= TOML['options']['toot_cap']:
            logging.info('Cap of number of posts reached. Stopping.')
            break

    logging.info(str(posted_count) + ' toots posted. Finished successfully.')

    # Evaluate excess records in database
    excess_count = 0

    db.execute('SELECT count(*) FROM toots WHERE feed_title=?', (feed_title,))
    db_count = db.fetchone()
    if db_count is not None:
        excess_count = db_count[0] - MAX_REC_COUNT

    # Delete excess records
    if excess_count > 0:
        db.execute('''
            WITH excess AS (
            SELECT id
            FROM toots
            WHERE feed_title = ?
            ORDER BY timestamp ASC
            LIMIT ?
            )
            DELETE from toots
            WHERE id IN excess''', (feed_title, excess_count))
        sql.commit()
        logging.debug('Deleted ' + str(excess_count) + ' records from database.')

    shutdown(start_time, log_file_name, 0)


if __name__ == '__main__':
    main(sys.argv)
