# feedtoot

feedtoot is a python script that downloads an RSS or Atom feed, processes the
new entries and posts a configurable summary of each as a toot on a Mastodon.

## Features

* Download feed from given url
* Detect and handle feeds in the RSS or Atom formats
* Specify how many words of the entry to post (summary)
* Upload the first image in the feed entry as attachment to toot
* Remember the entries that have aleady been posted to avoid double posting
* Specify maximum age of entry to be considered

## Usage

```text
feedtoot.py [-h] -f <.toml config file> [-p <mastodon password>]
```

## Arguments

|Switch |Description                                       | Example                    | Req  | Default     |
|-------|--------------------------------------------------|----------------------------|------|-------------|
| -f    | name of `.toml` config file                      | `feed.toml`                | Yes  | *required*  |
| -p    | Mastodon password                                | `my_Sup3r-S4f3/pw`         | Once | *See notes* |

## Notes

A password only needs to be provided for the first run. Once feedtoot has connected successfully to the
Mastodon host, an access token is saved in a `.secret` file named after the mastodon account, and a password is
no longer necessary (command-line switch `-p` is not longer required).

## Installation

Make sure python3 is installed.

feedtoot depends on the following python modules: `beautifulsoup4`, `Mastondon.py`, `feedparser`, `requests`
and `tomli` if python version < 3.11.

```sh
pip install beautifulsoup4 Mastodon.py feedparser requests tomli
```

You can then use crontab to schedule when the script runs. Hint: use [crontab.pro](https://crontab.pro) to
define the right syntax for the desired schedule.

## Logging

Log messages are sent to a `.log` file with the same name as the config file in the directory where feedtoot is run.
